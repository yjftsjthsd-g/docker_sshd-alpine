# ssh-alpine

A docker image built from Alpine Linux that just runs sshd.


## Use

Map in your desired `authorized_keys` file to `/authorized_keys`; I suggest
invoking and accessing the running container something like this:
```
sudo docker run -p 2222:22 --rm -v $HOME/.ssh/authorized_keys:/authorized_keys:ro sshd-alpine
ssh -o UserKnownHostsFile=/dev/null -o StrictHostKeyChecking=no -p 2222 root@localhost
pkill sshd  #run in container to kill it
```
(obviously change 2222 as desired)

Alternatively, you can place keys into `/authorized_keys.d/`

