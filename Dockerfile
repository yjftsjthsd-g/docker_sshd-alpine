FROM alpine AS base
LABEL maintainer "Brian Cole <docker@brianecole.com>"
RUN apk add --update --no-cache openssh bash shadow

# TODO this really isn't "nice" (read: secure), because it bakes the host keys into the container
RUN ["/usr/bin/ssh-keygen", "-t", "ed25519", "-f", "/etc/ssh/ssh_host_ed25519_key", "-C", "", "-N", ""]
RUN sed -i 's/#HostKey \/etc\/ssh\/ssh_host_ed25519_key/HostKey \/etc\/ssh\/ssh_host_ed25519_key/' /etc/ssh/sshd_config

RUN usermod -p \* root && usermod -U root

# create empty authorized_keys file
RUN ["/bin/mkdir", "-m", "700", "/root/.ssh"]
RUN ["/bin/touch", "/root/.ssh/authorized_keys"]
RUN ["/bin/chmod", "600", "/root/.ssh/authorized_keys"]

COPY ./entrypoint.sh /opt/entrypoint.sh

EXPOSE 22

USER root
#ENTRYPOINT ["/usr/sbin/sshd", "-D"]
ENTRYPOINT ["/bin/sh", "/opt/entrypoint.sh"]
