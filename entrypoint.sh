#!/bin/sh

set -e
#set -x

# populate ssh keys
test -n "${SSH_AUTHORIZED_KEY}" && echo "${SSH_AUTHORIZED_KEY}" >> /root/.ssh/authorized_keys
test -e /authorized_keys && cat /authorized_keys >> /root/.ssh/authorized_keys
test -d /authorized_keys.d && for keyfile in /authorized_keys.d/*
do
    cat "$keyfile" >> /root/.ssh/authorized_keys
done || true

# actually execute sshd
#exec /usr/sbin/sshd -D -e -d
exec /usr/sbin/sshd -D
